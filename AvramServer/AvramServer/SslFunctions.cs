﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvramServer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Security;
    using System.Net.Sockets;
    using System.Security.Authentication;
    using System.Security.Cryptography.X509Certificates;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;

    namespace LotanServer
    {
        public sealed class SslTcpServer
        {
            const int DATA_SIZE = 4096;


            static X509Certificate serverCertificate = null;
            public static Int32 ConvertBytesToInt(byte[] data)
            {
                return BitConverter.ToInt32(data, 0);
            }
            public static byte[] ConvertIntToByteArray(int intValue)
            {
                byte[] intBytes = BitConverter.GetBytes(intValue);
                if (BitConverter.IsLittleEndian)
                    Array.Reverse(intBytes);
                //byte[] result = intBytes;
                return intBytes;
            }
            public static void RunServer(string certificate)
            {
                serverCertificate = X509Certificate.CreateFromCertFile(certificate);

                TcpListener listener = new TcpListener(IPAddress.Any, 8081);
                listener.Start();
                while (true)
                {
                    Console.WriteLine("Waiting for a client to connect...");

                    TcpClient client = listener.AcceptTcpClient();
                    Thread thr = new Thread(new ParameterizedThreadStart(ProcessClient));
                    thr.Start(client);

                }
            }
            static void ProcessClient(object obj)
            {
                TcpClient client = (TcpClient)obj;

                SslStream sslStream = new SslStream(
                    client.GetStream(), false);
                // Authenticate the server but don't require the client to authenticate.
                try
                {
                    sslStream.AuthenticateAsServer(serverCertificate,
                        false, SslProtocols.Tls, true);

                    DisplaySecurityLevel(sslStream);
                    DisplaySecurityServices(sslStream);
                    DisplayCertificateInformation(sslStream);
                    DisplayStreamProperties(sslStream);


                    sslStream.ReadTimeout = 1500000;
                    sslStream.WriteTimeout = 1500000;

                    Console.WriteLine("Waiting for client message...");
                    #region demo lol
                    List<byte> list = new List<byte>();
                    for (int i = 0; i < 20; i++)
                        list.Add(1);
                    #endregion
                    while (true)
                    {
                        try
                        {
                            byte[] response = new byte[200];
                            byte[] data = ReadMessage(sslStream);
                            byte[] len = new byte[4];
                            len[0] = 4;
                            len[1] = 0;
                            len[2] = 0;
                            len[3] = 0;

                            response[0] = 0;
                            response[1] = 0;
                            response[2] = 0;
                            response[3] = 0;  //CKR_OK

                            if (data[0] == 1) // Initialize
                            {
                                Random rand_pool = new Random();
                                for (int i = 0; i < 20; i++)
                                    response[i + 4] = 9; // "random" psid
                                len[0] = 24;
                            }
                            else if (data[20] == 10) // OpenSession (because of psid)
                            {
                                response[4] = 0x04;
                                response[5] = 0x03;
                                response[6] = 0x02;
                                response[7] = 0x01;
                                len[0] = 8;
                            }
                            // Console.WriteLine("Client message{0}", System.Text.Encoding.Default.GetString(data));
                            List<byte> packet = new List<byte>();
                            packet.AddRange(response);
                            sslStream.Write(len, 0, 4);
                            sslStream.Write(packet.ToArray(), 0, len[0]);
                            // sslStream.Read(data, 0, 1);
                        }
                        catch
                        {
                            throw new Exception("Can't read!"); //sslStream.WriteByte(Convert.ToByte(CK_ERRORS.CK_UNABLE_TO_PROCESS_PACKET));
                        }

                    }
                }
                catch (AuthenticationException e)
                {
                    Console.WriteLine("Exception: {0}", e.Message);
                    if (e.InnerException != null)
                    {
                        Console.WriteLine("Inner exception: {0}", e.InnerException.Message);
                    }
                    Console.WriteLine("Authentication failed - closing the connection.");
                    sslStream.Close();
                    client.Close();
                    return;
                }
                finally
                {
                    sslStream.Close();
                    client.Close();
                }
            }
            static void DisplaySecurityLevel(SslStream stream)
            {
                Console.WriteLine("Cipher: {0} strength {1}", stream.CipherAlgorithm, stream.CipherStrength);
                Console.WriteLine("Hash: {0} strength {1}", stream.HashAlgorithm, stream.HashStrength);
                Console.WriteLine("Key exchange: {0} strength {1}", stream.KeyExchangeAlgorithm, stream.KeyExchangeStrength);
                Console.WriteLine("Protocol: {0}", stream.SslProtocol);
            }
            static void DisplaySecurityServices(SslStream stream)
            {
                Console.WriteLine("Is authenticated: {0} as server? {1}", stream.IsAuthenticated, stream.IsServer);
                Console.WriteLine("IsSigned: {0}", stream.IsSigned);
                Console.WriteLine("Is Encrypted: {0}", stream.IsEncrypted);
            }
            static void DisplayStreamProperties(SslStream stream)
            {
                Console.WriteLine("Can read: {0}, write {1}", stream.CanRead, stream.CanWrite);
                Console.WriteLine("Can timeout: {0}", stream.CanTimeout);
            }
            static void DisplayCertificateInformation(SslStream stream)
            {
                Console.WriteLine("Certificate revocation list checked: {0}", stream.CheckCertRevocationStatus);

                X509Certificate localCertificate = stream.LocalCertificate;
                if (stream.LocalCertificate != null)
                {
                    Console.WriteLine("Local cert was issued to {0} and is valid from {1} until {2}.",
                        localCertificate.Subject,
                        localCertificate.GetEffectiveDateString(),
                        localCertificate.GetExpirationDateString());
                }
                else
                {
                    Console.WriteLine("Local certificate is null.");
                }

                X509Certificate remoteCertificate = stream.RemoteCertificate;
                if (stream.RemoteCertificate != null)
                {
                    Console.WriteLine("Remote cert was issued to {0} and is valid from {1} until {2}.",
                        remoteCertificate.Subject,
                        remoteCertificate.GetEffectiveDateString(),
                        remoteCertificate.GetExpirationDateString());
                }
                else
                {
                    Console.WriteLine("Remote certificate is null.");
                }
            }
            static byte[] ReadMessage(SslStream sslStream)
            {
                DateTime cooler = DateTime.Now;
                byte[] lenght = new byte[4];
                List<byte> len = new List<byte>();
                int read = 0;
                while (read < 4)
                {
                    int r = 0;
                    r = sslStream.Read(lenght, 0, 4);
                    len.AddRange(lenght.SubArray(0, r));
                    read += r;
                }
                // sslStream.WriteByte(1);

                int packetLenght = BitConverter.ToInt32(len.ToArray(), 0);
                List<byte> packet = new List<byte>();
                byte[] data = new byte[DATA_SIZE];
                read = 0;
                while (read < packetLenght)
                {
                    int r = 0;
                    r = sslStream.Read(data, 0, packetLenght);
                    packet.AddRange(data.SubArray(0, r));
                    read += r;
                }
                // sslStream.WriteByte(1);
                return packet.ToArray();
            }
            private static void DisplayUsage()
            {
                Console.WriteLine("To start the server specify:");
                Console.WriteLine("serverSync certificateFile.cer");
                Environment.Exit(1);
            }
            public static int sMain(string[] args)
            {
                string certificate = null;
                if (args == null || args.Length < 1)
                {
                    DisplayUsage();
                }
                certificate = args[0];
                SslTcpServer.RunServer(certificate);
                return 0;
            }
        }

    }

}
