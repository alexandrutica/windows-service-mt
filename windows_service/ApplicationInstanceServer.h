#pragma once
#include "Server.h"
#include <WinSock2.h>
#include <string>
#include <vector>
#include <shared_mutex>
#include "SessionManager.h"
#include "PacketType.h"
#include "PacketStructs.h"
#include "TLSClient.h"

class ApplicationInstanceServer : Server
{
public:
	ApplicationInstanceServer() {}
	ApplicationInstanceServer(int port, bool loopBackToLocalHost = true) : Server(port,loopBackToLocalHost) 
	{
		connectToRemoteServer("config.txt");
	}
	ApplicationInstanceServer(SOCKET socket,SOCKADDR_IN sockaddr)
	{
		m_sListen = socket;
		m_addr = sockaddr;

		if (!connectedToServer)
		{
			ApplicationInstanceServer::connectedToServer = ApplicationInstanceServer::ConnectTokenServer();
;
			this->lastSendPacketType = PacketType::None;

			//TODO: watch it!!! See about that allocation
			this->m_clientPacket = std::make_shared<PS::ShortPacket>();
			this->m_serverPacket = std::make_shared<PS::ShortPacket>();
			signalError = false;
		}
	}

	bool ListenForNewConnection(std::shared_ptr<Connection> oldConnection,const int newPort);
private:
	bool ProcessPacket(std::shared_ptr<Connection> connection, PacketType packetType);
	bool GetString(std::shared_ptr<Connection> connection,std::string & str);
	bool receiveFullCommand(std::shared_ptr<Connection> connection,int totalSize,PacketType packetType,int processID);
	bool receiveAllToFile(std::shared_ptr<Connection> connection, int totalSize, PacketType packetType, int processID);
	bool receiveServerFullCommand(int totalSize);
	bool receiveServerAllToFile(std::shared_ptr<Connection> connection, int totalSize);
	bool serverSecureFileSend();
	bool serverSecureSend(unsigned char* sendBuffer, int totalSize);

	bool ProcessClientPacket();
	bool ProcessServerPacket();
	static bool ConnectTokenServer();
	static const char* temporaryFile() 
	{
		char tempfilename[10];
		return _itoa(ApplicationInstanceServer::instaceId,tempfilename,10);
	}
	static const char* getServerIP() { return "127.0.0.1"; }

private:
	static TLSClient tokenRemoteServer;
	static std::mutex masterMutex;
	
public:
	static int instaceId;
	static const int commandMaximumLength = 1024 * 1024 * 5; // 5 MB length allowed
	static const int commandChunkMaximumLength = 1024 * 5; // 5 MB length allowed
	static const int  portLowerRange = 1113; 
	static const int  portUpperRange = 10000;
	static const int serverPort = 1112;
	static bool connectedToServer;
private:
	static bool connectToRemoteServer(char *configFile);
	SessionManager* sessionManager;
	bool m_terminateAppThread;

	SOCKET m_appSocket;

	std::shared_ptr<Connection> m_clientConnection;
	std::shared_ptr<Connection> m_serverConnection;

	std::shared_ptr<PS::ShortPacket> m_serverPacket;
	std::shared_ptr<PS::ShortPacket> m_clientPacket;

	PacketType lastSendPacketType;

	unsigned char* shortPacketBuffer;

	int shortPacketSize;
	int processID;
	bool shortPacket;
	unsigned long currentHId;


	bool signalError;
};

