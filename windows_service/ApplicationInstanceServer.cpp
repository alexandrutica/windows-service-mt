#include "ApplicationInstanceServer.h"
#include <iostream>
#include <string>
#include <Ws2tcpip.h> //for inet_pton
#include "TLSClient.h"


bool ApplicationInstanceServer::connectedToServer = false;

std::mutex ApplicationInstanceServer::masterMutex;
TLSClient ApplicationInstanceServer::tokenRemoteServer;
int ApplicationInstanceServer::instaceId = 0;

bool ApplicationInstanceServer::ListenForNewConnection(std::shared_ptr<Connection> oldConnection,const int newPort)
{
	this->sessionManager = SessionManager::gInstance(); // get session instance

	ApplicationInstanceServer::instaceId++;
	this->m_terminateAppThread = false;

	unsigned char port[4];

	memcpy(port, &newPort, sizeof(newPort));
	sendall(oldConnection, (const char*)port, 4);

	int addrlen = sizeof(m_addr);

	listen(m_sListen, SOMAXCONN);

	SOCKET newConnectionSocket = accept(m_sListen, (SOCKADDR*)&m_addr, &addrlen); //Accept a new connection
	
	if (newConnectionSocket == INVALID_SOCKET)
	{
		closesocket(newConnectionSocket);
		return false;
	}


	m_appSocket = newConnectionSocket;

	this->m_clientConnection = std::make_shared<Connection>(newConnectionSocket);

	std::shared_ptr<Connection> newConnection(std::make_shared<Connection>(newConnectionSocket));
	m_connections.push_back(newConnection); 
	newConnection->m_ID = m_IDCounter; 
	m_IDCounter += 1; 


	std::cout << "Client Connected! ID:" << newConnection->m_ID << std::endl;

	// test .. remove please
	unsigned char toSend[20];
	std::string str = "Cripto";
	int size = 6;// htonl(6);
	
	
	memcpy(toSend , &size, 4);
	memcpy(toSend+4, str.c_str(), str.size());

	//unsigned char* recvBuffer;
	//int recvSize = 0;
	//std::string toRecv;
	


	while (true)
	{

		//get form client app
		if (this->ProcessClientPacket())
		{
			if (this->shortPacket == true)
			{
				this->masterMutex.lock();
			
				// send to server -- ssl
				tokenRemoteServer.send_packet(this->m_clientPacket->getContent(), this->m_clientPacket->getSize());
				// move to processServerPacket
				//this->m_serverPacket = tokenRemoteServer.receive_packet();
				//get server response
				this->ProcessServerPacket();

				this->masterMutex.unlock();

				/* Send the packet to client */
				/* First send the length */
				int32_t packetSize = m_serverPacket->getSize();
				this->sendall(this->m_clientConnection->m_socket,(const char*) &packetSize, sizeof(int32_t));

				/* Then send the actual packet */
				this->sendall(this->m_clientConnection->m_socket,(const char*) this->m_serverPacket->getContent(), this->m_serverPacket->getSize());
			}
			else
			{
				
			}
		}
		/*GetString(newConnection, toRecv);
		std::cout << toRecv << std::endl;
		sendall(newConnection, (const char *)toSend, 10);
		Sleep(5);*/
	}

	return true;
}

bool ApplicationInstanceServer::GetString(std::shared_ptr<Connection> connection,std::string & str)
{
	
	int32_t bufferlength; //Holds length of the message
	if (!Getint32_t(connection,bufferlength)) //Get length of buffer and store it in variable: bufferlength
		return false; //If get int fails, return false
	if (bufferlength == 0) return true;
	str.resize(bufferlength); //resize string to fit message
	return recvall(connection,&str[0], bufferlength);
}

bool ApplicationInstanceServer::ProcessPacket(std::shared_ptr<Connection> connection, PacketType packetType)
{
	return false;
}

/**
 * Used to receive payload from client application (dll) and 
 * prepare the package to be sent to server
 */
bool ApplicationInstanceServer::receiveFullCommand(std::shared_ptr<Connection> connection,int totalSize,PacketType packetType,int processID)
{
	unsigned char* recvBuffer;
	unsigned int badResponse = CK_OK;
	char badBuffer[8];
	unsigned int size_int32 = 4;
	int recvCursor = 0;

	memcpy(badBuffer, &size_int32, size_int32);

	if (packetType != PacketType::Initialize)
	{
		recvBuffer = new unsigned char[totalSize + PSID_LENGTH + 1];
		recvCursor = PSID_LENGTH;
	} else {
		recvBuffer = new unsigned char[totalSize + 1]; // 1 -- PCKS11 function identificator
	}

	if (ApplicationInstanceServer::commandMaximumLength >= totalSize) // if short
	{
		this->shortPacket = true;
		if (this->recvall(connection, (char*)recvBuffer + recvCursor + 1, totalSize))
		{
			int serverPacketSize = totalSize + recvCursor + 1;
			
			//verificare Hid
			if ( packetType == PacketType::CloseSession || packetType > PacketType::CloseAllSessions )
			{
				// daca exista hID in payload
				memcpy(&currentHId, recvBuffer + recvCursor + 1, sizeof(unsigned int));
				if (!this->sessionManager->hasHid(processID, currentHId))
				{
					badResponse = CKR_SESSION_HANDLE_INVALID;
					goto cleanup;
				}
			}

			//daca nu e initialize, NAT-am din pid in psid
			if (recvCursor > 0)
			{
				const unsigned char* psid = sessionManager->gPSid(processID);
				if (psid != nullptr)
					memcpy(recvBuffer, psid, PSID_LENGTH);
				else
					return false; // psid not found -- initialise
			}

			//pune fid 
			memcpy(recvBuffer + recvCursor, &packetType, 1);

			// HERE WAS THE SWITCH
			this->m_clientPacket.reset(new PS::ShortPacket((const unsigned char*)recvBuffer, serverPacketSize));

			//HERE WAS A SECURE SEND. Moved logic inside mutex
			return true;
		}
		else
			return false;
	}
	else
	{
		this->shortPacket = false;
		if (!this->receiveAllToFile(connection, totalSize,packetType,processID))
			return false;
	}

cleanup:
	delete[] recvBuffer;

	if (badResponse == CK_OK)
	{
		unsigned char ok[8] = { 0x04, 0x00, 0x00, 0x00, 0x00, 0x00,0x00,0x00 };
		this->sendall(this->m_clientConnection, (const char*)ok, sizeof(unsigned int));
		this->sendall(this->m_clientConnection, (const char*)ok + sizeof(unsigned int), sizeof(unsigned int));
		return true;
	}
	else {
		memcpy(badBuffer + size_int32, &badResponse, sizeof(unsigned int));

		this->sendall(this->m_clientConnection, (const char*)badBuffer, sizeof(unsigned int));
		this->sendall(this->m_clientConnection, (const char*)badBuffer + size_int32, sizeof(unsigned int));

		return false;
	}
}

bool ApplicationInstanceServer::receiveAllToFile(std::shared_ptr<Connection> connection, int totalSize,PacketType packetType, int processID)
{
	    unsigned char* recvBuffer = new unsigned char[ApplicationInstanceServer::commandChunkMaximumLength];
		FILE *fp = fopen(ApplicationInstanceServer::temporaryFile(), "wb+");

		if (fp == NULL)
		{
			return false;
		}

		unsigned char* packetHeader = nullptr;
		int recvCursor = 0;

		int int32_size = sizeof(int32_t);

		if (packetType != PacketType::Initialize)
		{
			packetHeader = new unsigned char[totalSize + int32_size + PSID_LENGTH + 1];
			recvCursor = PSID_LENGTH;
		}
		else
		{
			recvBuffer = new unsigned char[totalSize + int32_size + 1]; // 1 -- PCKS11 function identificator
		}

		int serverPacketSize = totalSize + recvCursor + 1;
		memcpy(packetHeader, &serverPacketSize,int32_size);

		if (recvCursor > 0)
		{
			const unsigned char* psid = sessionManager->gPSid(processID);

			if (psid != nullptr)
				memcpy(packetHeader + int32_size, psid, PSID_LENGTH);
			else
				return false;
		}

		memcpy(packetHeader + int32_size + recvCursor, &packetType, sizeof(int8_t));

		fwrite(packetHeader, 1, recvCursor + int32_size + 1, fp);
		int tempReceivedLength = 0;

		int fullCkunks = totalSize / ApplicationInstanceServer::commandChunkMaximumLength;

		if (this->recvall(connection, (char*)recvBuffer, ApplicationInstanceServer::commandChunkMaximumLength))
		{
			fwrite(recvBuffer, 1, ApplicationInstanceServer::commandChunkMaximumLength, fp);
			if (((packetType >= PacketType::Initialize && packetType <= PacketType::OpenSession) || packetType == PacketType::CloseAllSessions))
				memcpy(&currentHId, recvBuffer, sizeof(unsigned long));
		}
		else
			return false;

		for (int i = 0; i < fullCkunks -1; i++)
		{
			if (this->recvall(connection, (char*)recvBuffer, ApplicationInstanceServer::commandChunkMaximumLength))
			{
				fwrite(recvBuffer, 1, ApplicationInstanceServer::commandChunkMaximumLength, fp);
			}
			else
				return false;
		}


		// write the last (incomplete) chunk to file
		int lastChunkLength = totalSize - ApplicationInstanceServer::commandChunkMaximumLength * fullCkunks;
		if (this->recvall(connection, (char*)recvBuffer, lastChunkLength))
		{
			fwrite(recvBuffer, 1, lastChunkLength, fp);
			return true;
		}
		else
			return false;

		fclose(fp);
		delete[] packetHeader;
		delete[] recvBuffer;

		return true;
}

bool ApplicationInstanceServer::receiveServerFullCommand(int totalSize)
{
	unsigned char* recvBuffer = new unsigned char[totalSize + sizeof(int32_t)];

	// pentru test
	// unsigned char testPsid[22] = " 12345678901234567890";
	// testPsid[0] = 0;
	
	if (this->lastSendPacketType == PacketType::Initialize)
	{

	}

	if (ApplicationInstanceServer::commandMaximumLength >= totalSize)
	{
		if ( true ) 
		{
			tokenRemoteServer.recv_data((char*)recvBuffer, totalSize);
			//memcpy(recvBuffer + sizeof(int), testPsid, totalSize);
			this->shortPacket = true;
			unsigned char flag = CK_OK;
			if (memcmp(recvBuffer, &flag, 1) == 0)
			{
				// memcpy(recvBuffer, &totalSize, sizeof(int32_t));

				switch (this->lastSendPacketType)
				{
				case PacketType::Initialize:
				{
					/* recvBuffer will be like:
					
					------------------------------------------
					|  ERR_CODE  |           PSID            |
					|     4      |            20             |
					------------------------------------------

					*/
					this->sessionManager->addPid(this->processID);
					this->sessionManager->sPSid(this->processID, recvBuffer + 4); //received data skipping CK_OK status
					
					this->m_serverPacket.reset(new PS::ShortPacket(recvBuffer, 4));
				} break;
				
				case PacketType::OpenSession:
				{
					unsigned int hId = 0;
					/* recvBuffer will be like:
					
					--------------------------
					|  ERR_CODE  |    HID    |
					|     4      |     4     |
					--------------------------

					*/
					memcpy(&hId, recvBuffer + 4, sizeof(unsigned int));
					this->sessionManager->addHid(this->processID, hId);
					this->m_serverPacket.reset(new PS::ShortPacket(recvBuffer, totalSize));
				} break;

				case PacketType::Finalise :
				{
					this->sessionManager->removePid(this->processID);
					this->m_serverPacket.reset(new PS::ShortPacket(recvBuffer, totalSize));
				} break;

				case PacketType::CloseSession:
				{
					this->sessionManager->removeHid(this->processID,this->currentHId);
					this->m_serverPacket.reset(new PS::ShortPacket(recvBuffer, totalSize));
				} break;

				case PacketType::CloseAllSessions:
				{
					this->sessionManager->removeAllHid(this->processID);
					this->m_serverPacket.reset(new PS::ShortPacket(recvBuffer, totalSize));
				} break;

				default:
					/* recvBuffer will be like:
					
					--------------------------------
					|  ERR_CODE  |     Payload     |
					|     4      |    ~variable~   |
					--------------------------------

					*/
					this->m_serverPacket.reset(new PS::ShortPacket(recvBuffer, totalSize));
					break;
				}
			}
			else
				return false;
		}
	}
	else
	{
		/* NOT TESTED !!! */
		this->shortPacket = false;
		this->receiveServerAllToFile(this->m_serverConnection, totalSize);
	}
	delete[] recvBuffer;
	return true;
}

bool ApplicationInstanceServer::receiveServerAllToFile(std::shared_ptr<Connection> connection, int totalSize)
{
	unsigned char* recvBuffer = new unsigned char[ApplicationInstanceServer::commandChunkMaximumLength];
	FILE *fp = fopen(ApplicationInstanceServer::temporaryFile(), "wb+");

	if (fp == NULL)
	{
		return false;
	}

	unsigned char* packetHeader = nullptr;

	int int32_size = sizeof(int32_t);


	memcpy(packetHeader, &totalSize, int32_size);


	fwrite(packetHeader, 1, int32_size , fp);


	int fullCkunks = totalSize / ApplicationInstanceServer::commandChunkMaximumLength;

	for (int i = 0; i < fullCkunks; i++)
	{
		if (this->recvall(connection, (char*)recvBuffer, ApplicationInstanceServer::commandChunkMaximumLength))
		{
			fwrite(recvBuffer, 1, ApplicationInstanceServer::commandChunkMaximumLength, fp);
		}
		else
			return false;
	}

	// write the last (incomplete) chunk to file
	int lastChunkLength = totalSize - ApplicationInstanceServer::commandChunkMaximumLength * fullCkunks;
	if (this->recvall(connection, (char*)recvBuffer, lastChunkLength))
	{
		fwrite(recvBuffer, 1, lastChunkLength, fp);
		return true;
	}
	else
		return false;

	fclose(fp);
	delete[] packetHeader;
	delete[] recvBuffer;

	return true;
}

bool ApplicationInstanceServer::serverSecureFileSend()
{
	return false;
}

bool ApplicationInstanceServer::serverSecureSend(unsigned char * sendBuffer, int totalSize)
{
	// send the file to the token's server 
	//this->m_tokenRemoteServer
	//this->temporaryFile
	tokenRemoteServer.send_data((char*)sendBuffer, totalSize);
	return true;
}

bool ApplicationInstanceServer::ProcessClientPacket()
{
	std::int32_t totalSize = 0;
	PacketType packetType;

	std::int32_t processID;

	// Get the total size of the incomming packet
	if (!this->Getint32_t(this->m_clientConnection, totalSize))
	{
		this->m_terminateAppThread = true;
		return false;
	}

	// get process ID
	if (!this->Getint32_t(this->m_clientConnection, processID))
	{
		this->m_terminateAppThread = true;
		return false;
	}

	this->processID = processID;

	// get packet type
	if (!this->GetBytePacketType(this->m_clientConnection, packetType))
	{
		this->m_terminateAppThread = true;
		this->lastSendPacketType = (PacketType)packetType;

		return false;
	}

	this->lastSendPacketType = (PacketType)packetType;

	// get the rest
	if (!this->receiveFullCommand(this->m_clientConnection, totalSize - sizeof(int32_t) - 1, packetType, processID))
	{
		this->m_terminateAppThread = true;
		return false;
	}
	return true;
}

bool ApplicationInstanceServer::ProcessServerPacket()
{
	int totalSize = 0;
	/* Receive server packet size */
	tokenRemoteServer.recv_data(&totalSize, sizeof(totalSize));

	if (receiveServerFullCommand(totalSize))
	{
		this->m_terminateAppThread = true;
		return false;
	}
	return true;

}

bool ApplicationInstanceServer::ConnectTokenServer()
{

	connectToRemoteServer("config.txt");
	return true;
}

bool ApplicationInstanceServer::connectToRemoteServer(char * configFile)
{
	FILE *fd;
	int N;
	char ip[16];
	char port[6];

	fd = fopen(configFile, "r");

	fscanf(fd, "N=%d\n", &N);
	
	for (int i = 0; i < N; i++)
	{
		fscanf(fd, "ip=%16[^;];port=%6[^;]", ip, port);
		if (tokenRemoteServer.connect_to(ip, port) == true)
		{
			break;
		}
	}

	return true;
}


