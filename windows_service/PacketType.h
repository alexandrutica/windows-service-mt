#pragma once

#define PSID_LENGTH 20
#define CK_OK 0
#define CKR_SESSION_HANDLE_INVALID 0x000000b3

enum class PacketType
{
	None = 0,
	Initialize = 1,
	Finalise = 2,
	GetFunctionList = 3,
	GetInfo = 4,
	GetSlotList = 5,
	GetSlotinfo = 6,
	GetMechanismList = 7,
	GetMechanisminfo = 8,
	GetTokenInfo = 9,
	OpenSession = 10,
	CloseSession = 11,
	CloseAllSessions = 12,
};
