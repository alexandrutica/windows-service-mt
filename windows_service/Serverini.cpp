#include "Server.h"
#include <WS2tcpip.h>
#include <iostream>
#pragma comment(lib,"ws2_32.lib") //Required for WinSock

Server::Server(int port, bool loopBacktoLocalHost) 
{
	//Winsock Startup
	WSAData wsaData;
	WORD DllVersion = MAKEWORD(2, 1);
	if (WSAStartup(DllVersion, &wsaData) != 0) 
	{
		MessageBoxA(0, "WinSock startup failed", "Error", MB_OK | MB_ICONERROR);
		exit(1);
	}
	if (loopBacktoLocalHost) 
		inet_pton(AF_INET, "127.0.0.1", &m_addr.sin_addr.s_addr);
	else
		m_addr.sin_addr.s_addr = htonl(INADDR_ANY); 

	m_addr.sin_port = htons(port); //Port
	m_addr.sin_family = AF_INET; //IPv4 Socket

	m_sListen = socket(AF_INET, SOCK_STREAM, 0); 
	if (bind(m_sListen, (SOCKADDR*)&m_addr, sizeof(m_addr)) == SOCKET_ERROR)
	{
		std::string ErrorMsg = "Failed to bind the address to our listening socket. Winsock Error:" + std::to_string(WSAGetLastError());
		MessageBoxA(0, ErrorMsg.c_str(), "Error", MB_OK | MB_ICONERROR);
		exit(1);
	}
	if (listen(m_sListen, SOMAXCONN) == SOCKET_ERROR)
	{
		std::string ErrorMsg = "Failed to listen on listening socket. Winsock Error:" + std::to_string(WSAGetLastError());
		MessageBoxA(0, ErrorMsg.c_str(), "Error", MB_OK | MB_ICONERROR);
		exit(1);
	}
	
	m_IDCounter = 0;
}

bool Server::ListenForNewConnection()
{
	int addrlen = sizeof(m_addr);
	SOCKET newConnectionSocket = accept(m_sListen, (SOCKADDR*)&m_addr, &addrlen); 
	if (newConnectionSocket == 0)
	{
		std::cout << "Failed to accept the client's connection." << std::endl;
		return false;
	}
	else 
	{
		std::lock_guard<std::shared_mutex> lock(m_mutex_connectionMgr); 
		std::shared_ptr<Connection> newConnection(std::make_shared<Connection>(newConnectionSocket));
		m_connections.push_back(newConnection); 
		newConnection->m_ID = m_IDCounter; 
		m_IDCounter += 1; 
		std::cout << "Client Connected! ID:" << newConnection->m_ID << std::endl;
		std::thread CHT(ClientHandlerThread, std::ref(*this), newConnection);
		CHT.detach();
		m_threads.push_back(&CHT);
		return true;
	}
}

Server::~Server()
{
	m_terminateThreads = true;
	for (std::thread * t : m_threads) 
	{
		t->join();
	}
}
