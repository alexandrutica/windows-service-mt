#include "Packet.h"
#include <cstdint> //Required to use std::std::int32_t
#include <WinSock2.h> //for htonl
Packet::Packet()
{
}

Packet::Packet(const unsigned char * dataBuffer, const int size)
{
	Append(dataBuffer, size);
}

Packet::Packet(const std::shared_ptr<Packet> p)
{
	Append(p);
}

void Packet::Append(const std::shared_ptr<Packet> p)
{
	Append((const unsigned char*)&(p->m_buffer[0]), p->m_buffer.size());
}

Packet::Packet(PacketType pt)
{
	Append(pt);
}

void Packet::Append(const unsigned char * dataBuffer, const int size)
{
	m_buffer.insert(m_buffer.end(), dataBuffer, dataBuffer + size);
}

void Packet::Append(const Packet & p) //Allocate new block for buffer
{
	Append((const unsigned char*)&p.m_buffer, p.m_buffer.size());
}

void Packet::Append(std::int32_t int32)
{
	std::int32_t val = htonl((std::int32_t)int32);
	Append((const unsigned char*)&val, sizeof(std::int32_t));
}

void Packet::Append(PacketType pt)
{
	Append((std::int32_t)pt);
}

void Packet::Append(std::size_t s)
{
	Append((std::int32_t)s);
}
