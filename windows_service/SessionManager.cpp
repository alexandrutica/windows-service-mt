#include <unordered_set>
#include <unordered_map>
using namespace std;

#include "SessionManager.h"


SessionManager::SessionManager()
{
	tabel.clear();
}


SessionManager::~SessionManager()
{
	tabel.clear();
}
SessionManager::SessionManager(const SessionManager &) {}
SessionManager& SessionManager::operator=(const SessionManager&) { return *instanta; }

bool SessionManager::hasPid(int Pid)
{
	if (tabel.count(Pid) == 1)
		return true;
	return false;
}
bool SessionManager::removePid(int Pid)
{
	if (hasPid(Pid) == false)
		return false;
	tabel.erase(Pid);
	return true;
}
bool SessionManager::addPid(int Pid)
{
	if (hasPid(Pid) == true)
		return false;
	tabel.insert({ Pid, SessionInfo() });
	tabel.find(Pid)->second.Hid.clear();
	return true;
}
const unsigned char * SessionManager::gPSid(int Pid)
{
	if (hasPid(Pid) == false)
		return nullptr;
	return tabel.find(Pid)->second.PSid;
}
bool SessionManager::sPSid(int Pid, unsigned char * PSid)
{
	if (hasPid(Pid) == false)
		return false;
	memcpy(tabel.find(Pid)->second.PSid, PSid, 20);
	return true;
}
bool SessionManager::hasHid(int Pid, unsigned long Hid)
{
	if (hasPid(Pid) == false)
		return false;
	if (tabel.find(Pid)->second.Hid.count(Hid) == 1)
		return true;
	return false;
}
bool SessionManager::addHid(int Pid, unsigned long Hid)
{
	if (hasPid(Pid) == false)
		return false;
	if (hasHid(Pid, Hid) == true)
		return false;
	tabel.find(Pid)->second.Hid.insert(Hid);
	return true;
}
bool SessionManager::removeHid(int Pid, unsigned long Hid)
{
	if (hasPid(Pid) == false)
		return false;
	if (hasHid(Pid, Hid) == false)
		return false;
	tabel.find(Pid)->second.Hid.erase(Hid);
	return true;
}
bool SessionManager::removeAllHid(int Pid)
{
	if (hasPid(Pid) == false)
		return false;
	tabel.find(Pid)->second.Hid.clear();
	return true;
}
SessionManager* SessionManager::gInstance()
{
	if (instanta == nullptr)
		instanta = new SessionManager();
	return instanta;
}
void SessionManager::dInstance()
{
	if (instanta != nullptr)
		delete instanta;
	instanta = nullptr;
}

SessionManager * SessionManager::instanta = nullptr;


/*int main()
{
SessionManager * d = SessionManager::gInstance();
d->addPid(12);
d->addHid(12, 1231);
d->addHid(2, 12);
d->addHid(12, 2);
d->removePid(233);
d->removeAllHid(12);
d->removePid(12);
//d.insert({ 12, SessionInfo() });
d->dInstance();
return 0;
}
*/
SessionManager::SessionInfo::~SessionInfo()
{
	Hid.clear();
}
