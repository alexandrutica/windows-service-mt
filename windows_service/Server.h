#pragma once

#include <WinSock2.h>
#include <string>
#include <vector>
#include <shared_mutex>
#include "PacketManager.h" //for PacketManager class
#include "PacketType.h"

class Connection
{
public:
	Connection(SOCKET socket) : m_socket(socket){}
	SOCKET m_socket;
	PacketManager m_pm;
	int m_ID = 0;
};

class Server
{
public: 
	Server() {};
	Server(SOCKET socket,SOCKADDR_IN sockaddr) : m_sListen(socket), m_addr(sockaddr){}
	Server(int port, bool loopBacktoLocalHost = true);
	~Server();
	virtual bool ListenForNewConnection();
protected:
	bool sendall(std::shared_ptr<Connection> connection, const char * data, const int totalBytes);
	bool sendall(SOCKET connection, const char * data, const int totalBytes);
	bool recvall(std::shared_ptr<Connection> connection, char * data, int totalBytes);
	bool Getint32_t(std::shared_ptr<Connection> connection, std::int32_t & int32_t);
	bool Getint8_t(std::shared_ptr<Connection> connection, std::int8_t & int8_t);
	bool GetPacketType(std::shared_ptr<Connection> connection, PacketType & packetType);
	bool GetBytePacketType(std::shared_ptr<Connection> connection, PacketType & packetType);
	int GetString(std::shared_ptr<Connection> connection, unsigned char** str,int* size);
	virtual bool ProcessPacket(std::shared_ptr<Connection> connection, PacketType packetType);
	void DisconnectClient(std::shared_ptr<Connection> connection); 
	static void ClientHandlerThread(Server & server, std::shared_ptr<Connection> connection);

protected: 
	std::vector<std::shared_ptr<Connection>> m_connections;
	std::shared_mutex m_mutex_connectionMgr; 
	int m_IDCounter = 0;
	SOCKADDR_IN m_addr; 
	SOCKET m_sListen;
	bool m_terminateThreads = false;
	std::vector<std::thread*> m_threads; 
};