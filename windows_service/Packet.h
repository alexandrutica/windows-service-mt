#pragma once
#include "PacketType.h"
#include <cstdint>
#include <vector>
#include <memory>
class Packet
{
public:
	Packet();
	Packet(const unsigned char * buffer, const int size); 
												
	Packet(const PacketType p); 
	Packet(const std::shared_ptr<Packet> p);
	void Append(const std::shared_ptr<Packet> p);
	void Append(const PacketType p);
	void Append(const std::int32_t int32);
	void Append(const std::size_t p);
	void Append(const Packet & p);
	void Append(const unsigned char * buffer, const int size); 
	std::vector<int8_t> m_buffer; 
};