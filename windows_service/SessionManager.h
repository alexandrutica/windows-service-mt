#pragma once
#include <unordered_set>
#include <unordered_map>

using namespace std;




class SessionManager
{
private:
	static SessionManager * instanta;
	struct SessionInfo
	{
		unordered_set<unsigned long> Hid;
		unsigned char PSid[20];
		~SessionInfo();
	};

	unordered_map<int, SessionInfo> tabel;

	SessionManager();
	~SessionManager();
	SessionManager(const SessionManager &);
	SessionManager& operator=(const SessionManager&);

public:

	bool hasPid(int Pid);
	bool removePid(int Pid);
	bool addPid(int Pid);
	const unsigned char * gPSid(int Pid);
	bool sPSid(int Pid, unsigned char * PSid);
	bool hasHid(int Pid, unsigned long Hid);
	bool addHid(int Pid, unsigned long Hid);
	bool removeHid(int Pid, unsigned long Hid);
	bool removeAllHid(int Pid);
	static SessionManager* gInstance();
	static void dInstance();
};