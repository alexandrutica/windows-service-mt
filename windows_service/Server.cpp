#include "Server.h"
#include <iostream>
#include "PacketStructs.h"
#include <Ws2tcpip.h> //for inet_pton
#include "ApplicationInstanceServer.h"

#pragma comment(lib,"ws2_32.lib") //Required for WinSock

void Server::ClientHandlerThread(Server & server, std::shared_ptr<Connection> connection) //ID = the index in the SOCKET connections array
{
	char* hostname = "127.0.0.1";

	int portBeginRange = ApplicationInstanceServer::portLowerRange;
	int portEndRange = ApplicationInstanceServer::portUpperRange;
	int foundPort = 0;
	
	SOCKET tempSocket = socket(AF_INET, SOCK_STREAM, 0);
	u_long iMode = 0;

	SOCKADDR_IN tempAddr;
	inet_pton(AF_INET, hostname, &tempAddr.sin_addr.s_addr); 

	for (int i = portBeginRange; i < portEndRange; i++)
	{
		inet_pton(AF_INET, hostname, &tempAddr.sin_addr.s_addr);
		tempAddr.sin_port = htons(i); //Port 
		tempAddr.sin_family = AF_INET; //IPv4 Socket
		ioctlsocket(tempSocket, FIONBIO, &iMode);
		//bind(tempSocket, (SOCKADDR*)&tempAddr, sizeof(tempAddr)
		if ( ::bind(tempSocket,(const SOCKADDR*)&tempAddr,sizeof(tempAddr)) == SOCKET_ERROR) 
		{
			std::cout << "first try failed!\n";
		}
		else
		{
			foundPort = i;
			std::cout << "I found port " << i << " for the new application!\n";
			break;
		}

	}
	ApplicationInstanceServer applicationInstanceServer(tempSocket,tempAddr);
	applicationInstanceServer.ListenForNewConnection(connection,foundPort);
	server.DisconnectClient(connection);
}

void Server::DisconnectClient(std::shared_ptr<Connection> connection) //Disconnects a client and cleans up socket if possible
{
	std::lock_guard<std::shared_mutex> lock(m_mutex_connectionMgr); //Lock connection manager mutex since we are possible removing element(s) from the vector
	connection->m_pm.Clear(); //Clear out all remaining packets in queue for this connection
	closesocket(connection->m_socket); //Close the socket for this connection
	m_connections.erase(std::remove(m_connections.begin(), m_connections.end(), connection)); //Remove connection from vector of connections
	std::cout << "Cleaned up client: " << connection->m_ID << "." << std::endl;
	std::cout << "Total connections: " << m_connections.size() << std::endl;
}