#pragma once
#include "PacketType.h"
#include "Packet.h"
#include <string> 
#include <memory> 

namespace PS 
{

	class SimpleMessage
	{
	public:
		SimpleMessage(const unsigned char* message, int size);
	private:
		unsigned char* m_message;
		int size = 0;
	};

	class ShortPacket
	{
	public:
		ShortPacket();
		ShortPacket(const unsigned char* message, int size);
		unsigned char* getContent();
		int getSize();
		~ShortPacket();
	private:
		unsigned char* m_message = nullptr;
		int size = 0;
	};

}