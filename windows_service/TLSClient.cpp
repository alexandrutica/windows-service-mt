
#include <openssl\ssl.h>

#include "TLSClient.h"



TLSClient::TLSClient(){
	ctx =		nullptr;
	serverBio = nullptr;
	ssl =		nullptr;
	flags = SSL_OP_ALL | SSL_OP_NO_SSLv2 | SSL_OP_NO_SSLv3 | SSL_OP_NO_COMPRESSION;
	initialize();
}

void TLSClient::initialize()
{
	sslMethod = TLS_method();
	ctx = SSL_CTX_new(sslMethod);
	SSL_CTX_set_options(ctx, flags);
	serverBio = BIO_new_ssl_connect(ctx);
	outputBio = BIO_new_fp(stdout, BIO_NOCLOSE);
}

void TLSClient::send_data(void *buffer, int buffer_len)
{
	BIO_write(serverBio, buffer, buffer_len);
}

int TLSClient::recv_data(void *buffer, int max_len)
{
	if (buffer == NULL || max_len == 0) {
		return -1;
	}
	
	int bytesReceived = 0;

	while (bytesReceived < max_len) {
		bytesReceived += BIO_read(serverBio, (char*)buffer + bytesReceived, max_len - bytesReceived);
	}
	return bytesReceived;
}

void TLSClient::finalize()
{
}

/*TODO: Error handling*/
bool TLSClient::connect_to(char* host, char* port)
{
	char socketString[22];
	snprintf(socketString, 22, "%s:%s", host, port);
	BIO_set_conn_hostname(serverBio, socketString);
	BIO_get_ssl(serverBio, &ssl);

	SSL_set_cipher_list(ssl, PREFERRED_CIPHERS);
	SSL_set_tlsext_host_name(ssl, host);

	outputBio = BIO_new_fp(stdout, BIO_NOCLOSE);
	BIO_do_connect(serverBio);
	BIO_do_handshake(serverBio);

	return true;
}

void TLSClient::disconnect()
{
}


std::shared_ptr<PS::ShortPacket> TLSClient::receive_packet()
{
	int32_t packet_size = 0;
	recv_data(&packet_size, sizeof(int32_t));

	unsigned char* packet_buffer = new unsigned char[packet_size];

	recv_data(packet_buffer, packet_size);
	
	PS::ShortPacket* packet = new PS::ShortPacket(packet_buffer, packet_size);

	return std::shared_ptr<PS::ShortPacket>(packet);
}

void TLSClient::send_packet(void * buffer, int buffer_len)
{
	/* Send the length of the packet */
	send_data(&buffer_len, sizeof(buffer_len));

	/* Send the actual packet */
	send_data(buffer, buffer_len);
}

TLSClient::~TLSClient()
{
	finalize();
}
