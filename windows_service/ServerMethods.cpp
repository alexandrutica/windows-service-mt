#include "Server.h"
#include "PacketStructs.h"
#include <iostream>
#pragma comment(lib,"ws2_32.lib") //Required for WinSock

bool Server::recvall(std::shared_ptr<Connection> connection, char * data, int totalbytes)
{
	int bytesReceived = 0; //Holds the total bytes received
	while (bytesReceived < totalbytes) //While we still have more bytes to recv
	{
		int retnCheck = recv(connection->m_socket, data + bytesReceived, totalbytes - bytesReceived, 0); 
		if (retnCheck < 0)
		{
			printf("Error while sendiing %d\n", WSAGetLastError());
			return false;
		}
		std::cout << "Getting bytes "<< retnCheck<<std::endl;
		if (retnCheck == SOCKET_ERROR || retnCheck == 0) 
			return false; 
		bytesReceived += retnCheck; 
	}
	return true; 
}


bool Server::sendall(std::shared_ptr<Connection>   connection, const char * data, const int totalBytes)
{
	int bytesSent = 0; 
	while (bytesSent < totalBytes) 
	{
		int retnCheck = send(connection->m_socket, data + bytesSent, totalBytes - bytesSent, 0);
		if (retnCheck == SOCKET_ERROR) 
			return false; 
		bytesSent += retnCheck; 
	}
	return true; 
}

bool Server::sendall(SOCKET  connection, const char * data, const int totalBytes)
{
	int bytesSent = 0;
	while (bytesSent < totalBytes)
	{
		int retnCheck = send(connection, data + bytesSent, totalBytes - bytesSent, 0);
		if (retnCheck == SOCKET_ERROR)
			return false;
		bytesSent += retnCheck;
	}
	return true;
}

bool Server::Getint32_t(std::shared_ptr<Connection> connection, std::int32_t & int32_t)
{
	
	if (!recvall(connection, (char*)&int32_t, sizeof(std::int32_t)))
		return false; 
	//int32_t = ntohl(int32_t); 
	return true;
}

bool Server::Getint8_t(std::shared_ptr<Connection> connection, std::int8_t & int8_t)
{

	if (!recvall(connection, (char*)&int8_t, sizeof(std::int8_t)))
		return false;
	//int8_t = ntohl(int8_t);
	return true;
}


bool Server::GetPacketType(std::shared_ptr<Connection> connection, PacketType & packetType)
{
	std::int32_t packettype_int;
	if (!Getint32_t(connection, packettype_int)) 
		return false; 
	packetType = (PacketType)packettype_int;
	return true;
}


bool Server::GetBytePacketType(std::shared_ptr<Connection> connection, PacketType & packetType)
{
	std::int8_t packettype_int;
	if (!Getint8_t(connection, packettype_int))
		return false;
	packetType = (PacketType)packettype_int;
	return true;
}

bool Server::ProcessPacket(std::shared_ptr<Connection> connection, PacketType packetType)
{
	return false;
}


int Server::GetString(std::shared_ptr<Connection> connection, unsigned char** message,int * size)
{
	std::int32_t bufferlength; 
	if (!Getint32_t(connection, bufferlength)) 
		return false; 
	if (bufferlength == 0) return 0;

	(*size) = bufferlength;
	(*message) = new unsigned char[bufferlength + 1];
	message[bufferlength] = 0;
	recvall(connection, (char*)*message, bufferlength);
	return bufferlength;
}



